FROM python:latest

RUN groupadd -r devops && useradd -r -g devops user

COPY ./requirements.txt /app/requirements.txt

RUN pip install -r /app/requirements.txt

WORKDIR /app

COPY . /app

RUN chown -R user:devops /app

USER user:devops

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]