"""Test module."""
import unittest
from app import fun_sum

class Test(unittest.TestCase):
    """Test class"""

    def test_sum(self):
        """Test sum"""
        value = fun_sum(2,3)
        self.assertEqual(value, 5)

if __name__ == '__main__':
    unittest.main()
