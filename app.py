"""System module."""
from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def home():
    """function home"""
    return '<h1>Hi!</h1>'

@app.route('/calc')
def calc():
    """function calc"""
    arg_a = int(request.args.get('a'))
    arg_b = int(request.args.get('b'))

    return str(fun_sum(arg_a, arg_b))

def fun_sum(arg_a, arg_b):
    """function sum"""
    return arg_a + arg_b

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
